/* Philip T.L.C. Clausen Sep 2018 plan@dtu.dk */

/*
 * Copyright (c) 2017, Philip Clausen, Technical University of Denmark
 * All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *		http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include <stdio.h>
#include <string.h>
#include <sys/file.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char *argv[]) {
	
	int file;
	struct flock lock;
	
	if(argc != 2) {
		fprintf(stderr, "Need a file where EOF shall be determined.\n");
		return 1;
	}
	
	lock.l_type   = F_WRLCK;  /* read/write lock */
	lock.l_whence = SEEK_SET; /* beginning of file */
	lock.l_start  = 0;        /* offset from l_whence */
	lock.l_len    = 0;        /* length, 0 = to EOF */
	lock.l_pid    = getpid(); /* PID */
	
	/* open file */
	file = open(*++argv, O_RDONLY);
	if(file < 0) {
		fprintf(stderr, "Filename:\t%s\n", *argv);
		fprintf(stderr, "Error: %d (%s)\n", errno, strerror(errno));
		return errno;
	}
	fcntl(file, F_GETLK, &lock);
	close(file);
	
	if(lock.l_type == F_UNLCK) {
		fprintf(stdout, "YO\n");
	} else {
		fprintf(stdout, "NO\n");
	}
	
	return 0;
}
