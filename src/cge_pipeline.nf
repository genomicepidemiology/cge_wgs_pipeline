#!/usr/bin/env nextflow

package groovy.json
import java.io.File


Runtime.runtime.addShutdownHook {
  println "Shutting down..."
}

stage_pattern = "$params.stage/*.staged_json"
database_address = "$params.databases"

println "watchPath: $stage_pattern"

if(params.noloop == "False")
  in_channel = Channel.watchPath(stage_pattern)
else
  in_channel = Channel.fromPath(stage_pattern)

process readStagedFile {

    executor 'local'

    input:
    val in_channel

    output:
    val json_file_1 into compressed_file1
    val json_file_2 into compressed_file2
    val f1 into uncompressed_file1
    val f2 into uncompressed_file2

    exec:
    println "File: ${in_channel}"
    fh = new File("${in_channel}")
    def jsonSlurper = new JsonSlurper()
    hash = jsonSlurper.parseText(fh.text)
    println "Hash: ${hash.file_1}"
    json_file_1 = hash.file_1
    json_file_2 = hash.file_2
    fh.delete()

    // Create uncompresse file names
    if(json_file_1.contains("."))
        f1 = json_file_1.substring(0, json_file_1.lastIndexOf('.'))
        f2 = json_file_2.substring(0, json_file_2.lastIndexOf('.'))
}

process gunzipFiles {


    input:
    val compressed_file1
    val compressed_file2
    val uncompressed_file1
    val uncompressed_file2

    output:
    val uncompressed_file1 into file1
    val uncompressed_file2 into file2

    """
    gunzip -c $compressed_file1 > $uncompressed_file1
    gunzip -c $compressed_file2 > $uncompressed_file2
    """
}

/*
    File channels
*/
file1_species = Channel.create()
file1_virulence = Channel.create()
file1_mlst = Channel.create()
file1_plasmidfinder = Channel.create()
file1_pmlst = Channel.create()
file1_resfinder = Channel.create()
file1.separate(file1_species, file1_virulence, file1_mlst, file1_plasmidfinder, file1_pmlst, file1_resfinder) { a -> [a, a, a, a, a, a] }

file2_virulence = Channel.create()
file2_resfinder = Channel.create()
file2.separate(file2_virulence, file2_resfinder) { a -> [a, a] }

process findSpecies {

    container 'genomicepidemiology/kmerfinder:3.0.2'

    input:
    val file1_species

    output:
    file "data.json" into species_json

    """
    kmerfinder.py -i $file1_species -o . -db $database_address/kmerfinder_db/bacteria.ATG -tax $database_address/kmerfinder_db/bacteria.name -x
    """
}

process extractSpecies {

    executor 'local'

    input:
    file species_json

    output:
    stdout species
    file "data.json" into tax_json

    """
    #!/usr/bin/env python3

    import json

    with open("$species_json", "r", encoding="utf-8") as json_fh:
      species_dict = json.load(json_fh)

    best_score = 0
    species = "unknown"

    for hit, val in species_dict["kmerfinder"]["results"]["species_hits"].items():

        score = int(val["Score"])

        if(score > best_score):
          best_score = score
          species = val["Species"]

    print(species.lower(), end='')
    """
}

/*
  Create species channels

  Each process that needs output from a previous process must create a channel
  and fork output from the previous process to that channel.

  It involves two steps.
  1. Create empty channel
  2a. If a fork already exists, add your channel to the fork.
  2b. If no fork exists you must create a new fork.
*/

// 1. Create empty channel
species_mlst = Channel.create()
species_resfinder = Channel.create()
species_extract_info = Channel.create()

// 2. Add channel to the fork or create new fork
//    * Add your channel as an argument to the "separate" method.
//    * Add an additional 'a' to the mapping function. It is important that the
//      mapping function creates a list equal in length to the amount of
//      channels.
//    * It is possible to manipulate the 'a' value. So instead of just writing
//      'a', one could write 'a+1' or 'a*a'
//      Manipulating 'a' values makes it important to know that the index
//      number of the arguments corresponds to the index number in the mapping
//      functions list.
species.separate(species_mlst, species_resfinder, species_extract_info) { a -> [a, a, a] }


process extractSpeciesInfo {
    /*
    Outputs a text string in json format.
    The output is a list.
    Zero-index is gram type.
    Other indeces are ordered taxonomy information from domain -> species. So
    that domain is at index 1 and species are the last index.
    */
    executor 'local'

    input:
    val species_extract_info

    when:
    species_extract_info != "unknown"

    output:
    stdout speciesinfo

    """
    #!/usr/bin/env python3

    import json
    from cgecore.organisminfo.species import Species
    from cgecore.organisminfo.gramstain import Gramstain

    info = Species("$species_extract_info")
    out_info = list(info.tax_tuple)

    gramdb = Gramstain()
    gram_info = gramdb.get("$species_extract_info", "?")

    out_info.insert(0, gram_info)

    print(json.dumps(out_info), end='')
    """
}


speciesinfo_virulence = Channel.create()
debug_virulence = Channel.create()
speciesinfo.separate(speciesinfo_virulence, debug_virulence) { a -> [a, a] }


process mlst {

  container 'genomicepidemiology/mlst:2.0.3'


  input:
  val file1_mlst
  val species_mlst

  when:
  species_mlst != "unknown"

  output:
  file "data.json" into mlst_json
  stdout mlst_out

  """
  MLST_SPECIES=`grep -i "$species_mlst" $database_address/mlst_db/config | cut -f 1 -`
  echo \$MLST_SPECIES
  mlst.py  -s \$MLST_SPECIES -p $database_address/mlst_db/ -x -o . -i $file1_mlst
  """

}


process virulence {

  container 'genomicepidemiology/virulencefinder:2.0.2'


  input:
  val speciesinfo_virulence
  val file1_virulence
  val file2_virulence

  when:
  species_virulence != "unknown"

  output:
  file "data.json" into virulence_json
  stdout virulence_out

  """
  virulencefinder.py -p $database_address/virulencefinder_db/ -x -o . -i $file1_virulence $file2_virulence --speciesinfo_json '$speciesinfo_virulence'
  """

}


process plasmidfinder {

  container 'genomicepidemiology/plasmidfinder:2.0.1'


  input:
  val file1_plasmidfinder

  output:
  file "data.json" into plasmidfinder_json
  stdout plasmidfinder_out

  """
  plasmidfinder.py -i $file1_plasmidfinder -o . -p $database_address/plasmidfinder_db/
  """

}


process pmlst {

  container 'genomicepidemiology/pmlst:2.0.1'


  input:
  val file1_pmlst

  output:
  file "data.json" into pmlst_json
  stdout pmlst_out

  """
  pmlst.py -p $database_address/pmlst_db/ -x -o . -i $file1_pmlst -s incf
  """

}



process resfinder {

  container 'genomicepidemiology/resfinder:beta.2'


  input:
  val file1_resfinder
  val file2_resfinder
  val species_resfinder

  output:
  stdout resfinder_out

  """
  /resfinder/run_resfinder.py --acquired --point -ifq $file1_resfinder $file2_resfinder -o . -s "$species_resfinder" -db_res $database_address/resfinder_db -db_point $database_address/pointfinder_db
  """

}


resfinder_out.subscribe {
  println it
}

virulence_out.subscribe {
  println it
}

debug_virulence.subscribe {
    println it
}
