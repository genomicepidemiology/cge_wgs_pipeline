#! /tools/bin/python3

import signal
import sys
import os
import subprocess
import time
import shutil
import json
import re
from argparse import ArgumentParser
import multiprocessing
import sqlite3

from python_module_seqfilehandler.SeqFileHandler import SeqFile

# TODO: Handle NextSeq
# TODO: Handle settings per isolate (Howto get them?)
#       Option between one config file per isolate or one single default file.
#       Config files per isolate should be created by helper script, user
#       should only need to define values different from defaults.
# TODO: Implement check for "zombie" files. Delete after period of time.
# TODO: Create functional test


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


# TODO: Can this be put into main to avoid global var?


class FileListener:
    """
    """
    def __init__(self, dir_path):
        self.fileeof = (os.path.dirname(os.path.realpath(__file__))
                        + "/fileEOF")
        self.dir = dir_path

    def listen(self):
        detected_files = []

        for root, dirs, files in os.walk(self.dir):
            for file in files:
                # Ignore files with number extensions, as they are partially
                # uploaded files.
                # if(re.search(r'\d$', file)):
                #    continue

                # Ignore files without gzip extension
                if(not re.search(r'.gz$', file)):
                    continue

                file_path = os.path.join(root, file)

                try:
                    file_complete_raw = subprocess.check_output(
                        [self.fileeof, file_path])
                    file_complete = file_complete_raw.decode('utf-8').strip()
                except subprocess.CalledProcessError:
                    file_complete = "NO"

                # File lock has been released
                if(file_complete == "YO"):
                    file_path = os.path.abspath(file_path)

                    if(not os.path.exists(file_path)):
                        print("! Could not find file: " + file_path)

                    detected_files.append(file_path)

        return detected_files


class Database():

    def __init__(self, sqlite_out):
        """
        """
        self.db_file = sqlite_out
        self.conn = sqlite3.connect(sqlite_out)
        cursor = self.conn.cursor()
        cursor.execute("""CREATE TABLE runs
                          (session_id text not null,
                           filename text not null,
                           status text not null,
                           file_1 text not null,
                           file_2 text,
                           primary key (session_id, filename))
                       """)

        self.conn.commit()

    def get_matching_filenames(self, entries):
        """
        """
        placeholder = '?'
        placeholders = ', '.join(placeholder for unused in entries)

        query = (""" SELECT session_id, filename, status
                     FROM runs
                     WHERE filename IN (%s)""" % placeholders)

        return self.conn.execute(query, filenames).fetchall()

    def close(self):
        self.conn.close()

    def commit(self):
        self.conn.commit()

def create_config(system,work_dir,database_dir):
    work_dir=os.path.abspath(work_dir)
    database_dir=os.path.abspath(database_dir)
    config_file= ("%s/%s_%s"
                 % (work_dir, "nextflow.config",system))
    if system == "local":
        executor = "local"
    else:
        executor = "pbs"
    with open(config_file, "w") as cf:
        cf.write("docker.enabled = true\n")
        cf.write("docker.runOptions = '-v "+str(work_dir)+"/:"+str(work_dir)
        +" -v "+str(database_dir)+"/:"+str(database_dir)+" -v "+str(work_dir)+
        ":/workdir'\nparams.databases = '"+str(database_dir)+"'")
        cf.write("\nprocess {\n\twithName: 'gunzipFiles' {\n\t\texecutor='"
        +str(executor)+"'\n\t\tcpus=1\n\t}"+"\n")
        cf.write("\twithName: 'findSpecies' {\n\t\texecutor='"+str(executor)+
        "'\n\t\tcpus=1\n\t}"+"\n")
        cf.write("\twithName: 'mlst' {\n\t\texecutor='"+str(executor)+
        "'\n\t\tcpus=1\n\t}"+"\n")
        cf.write("\twithName: 'virulence' {\n\t\texecutor='"+str(executor)+
        "'\n\t\tcpus=1\n\t}"+"\n")
        cf.write("\twithName: 'plasmidfinder' {\n\t\texecutor='"+str(executor)+
        "'\n\t\tcpus=1\n\t}"+"\n")
        cf.write("\twithName: 'pmlst' {\n\t\texecutor='"+str(executor)+
        "'\n\t\tcpus=1\n\t}"+"\n")
        cf.write("\twithName: 'resfinder' {\n\t\texecutor='"+str(executor)+
        "'\n\t\tcpus=1\n\t}"+"\n")
        cf.write("}")
    cf.close()

    return config_file

def run_pipeline(nextflow, stage, loop, conf):
    prg_dir = os.path.dirname(os.path.realpath(__file__))

    loop_cmd = ""
    logs_cmd = "-with-trace"
    if(loop is False):
        loop_cmd = " --noloop False "
        logs_cmd = ("-with-report log_report.html -with-trace "
                    "-with-timeline log_timeline.html -with-dag log_dag.html ")

    nf_cmd = ("{nf:s} run -c {conf:s} {dir}/cge_pipeline.nf {log:s}{loop:s} "
              "--stage {stage:s}"
              .format(nf=nextflow, dir=prg_dir, log=logs_cmd, loop=loop_cmd,
                      stage=stage, conf=conf))
    print(nf_cmd)

    # eprint("CMD: {}".format(nf_cmd))
    subprocess.run([nf_cmd], shell=True)


if __name__ == '__main__':
    parser = ArgumentParser()

    # General options
    parser.add_argument("-i", "--inputdir",
                        help="Directory to process",
                        default=("/data/docker-volumes/cge-web-server-dev/"
                                 "user-data/secure-upload/service_samples/"))
    parser.add_argument("-idb", "--input_database",
                        help="File to store sqlite3 db about input files.",
                        default="./test/init_db")
    parser.add_argument("-s", "--stage",
                        help="Directory in which json files will be written.",
                        default="/home/rkmo/init_pipeline/test/stage")
    parser.add_argument("-d", "--databases_directory",
                        help="Directory containing the database folders.")
    parser.add_argument("--loop",
                        action="store_true",
                        help="Keep listening for new files until user kills\
                              process with a SIGINT signal (ctrl+c)",
                        default=False)
    parser.add_argument("--nextflow",
                        help="Path to nextflow",
                        default="nextflow")
    parser.add_argument("--no_cleanup",
                        action="store_true",
                        help="Will not cleanup staged json files and sqlite\
                              database",
                        default=False)
    parser.add_argument("-r", "--runs",
                        help="Which system and how the pipeline work: TORQUE \
                        queueing, local and manual",
                        choices=["queue", "local", "manual"],
                        default="local")
    parser.add_argument("-config", "--config_file",
                        help="Path to the nextflow config file for running the\
                        pipeline manually")
    args = parser.parse_args()

    def cleanup(db):
        runtime_db.close()
        if(not args.no_cleanup):
            subprocess.run(["rm", db])
            # subprocess.run(["rm {}/*.staged_json".format(args.stage)],
            #                shell=True)

    def signal_term_handler(signal, frame):
        # global NEXTFLOW_PROC.send_signal(signal.CTRL_C_EVENT)
        # runtime_db.close()
        # subprocess.run(["rm", db])
        cleanup(args.input_database)
        pipeline_p.terminate()
        print("\nFile listener stopped.")
        sys.exit(0)

    args.stage = os.path.abspath(args.stage)

    # global NEXTFLOW_PROC = subprocess.popen(
    #    "nextflow run ./test/tutorial.nf")

    #Manage the different ways of running the pipeline through the config file
    if args.runs == "manual" and args.config_file == None:
        print("\nRequired nextflow config file not provided. Leaving.")
        sys.exit(0)

    if args.runs == "manual":
        config_nextflow = str(args.config_file)
    else:
        config_nextflow = create_config(args.runs,args.inputdir,args.databases_directory)
        print(config_nextflow)


    # Handle Ctrl+C
    signal.signal(signal.SIGINT, signal_term_handler)
    # Handle terminate signal
    signal.signal(signal.SIGTERM, signal_term_handler)
    # Create runtime DB
    runtime_db = Database(args.input_database)
    padding = "##########"

    eprint("{pad:s}{head:^25s}{pad:s}".format(
        pad=padding, head="Starting pipeline"))

    ctx = multiprocessing.get_context("spawn")
    pipeline_p = ctx.Process(target=run_pipeline,
                             args=(args.nextflow, args.stage, args.loop,config_nextflow))
    if(args.loop):
        pipeline_p.start()
        time.sleep(10)

    eprint("{pad:s}{head:^25s}{pad:s}".format(
        pad=padding, head="Starting file listener"))

    file_listener = FileListener(args.inputdir)
    pipeline_started = False

    while(True):
        eprint("# Listening for files")

        # Find input files
        detected_files = file_listener.listen()

        #
        # Validate found input files
        #

        detected_seqfiles = SeqFile.parse_files(detected_files,
                                                ignore_errors=True)

        input_files = {}

        for seqfile in detected_seqfiles:
            file_meta_dict = {}

            # Check if file is paired
            if(SeqFile.predict_pair(seqfile.path)):
                # Check if paired file exists
                if(seqfile.pe_file_reverse is None):
                    eprint("#! Pair is missing, ignore: " + str(seqfile.path))
                    continue

                file_meta_dict["file_2"] = seqfile.pe_file_reverse

            file_meta_dict["file_1"] = seqfile.path

            # TODO: Record pipeline parameters in file_meta_dict here.

            file_dir, filename = os.path.split(seqfile.path)
            session_id = file_dir.split("/")[-2]
            # eprint("# \t- found and validated: " + session_id + " " + filename)
            input_files[(session_id, filename)] = file_meta_dict

        #
        # Stage files
        #

        # Check if files are already registered
        # TODO: Check if identical filenames in different sessions is an issue
        filenames = [(j) for i, j in input_files.keys()]
        rows = runtime_db.get_matching_filenames(filenames)

        for r in rows:
            del input_files[(r[0], r[1])]
            # eprint("#\t- already staged: " + str((r[0], r[1])))

        # Register new files in the DB

        for (session_id, filename), vals in input_files.items():
            json_file = ("%s/%s_%s.staged_json"
                         % (args.stage, session_id, filename))
            with open(json_file, "w") as fh:
                json.dump(vals, fh)
                eprint("Wrote: {}".format(json_file))

            row = []
            row.append(session_id)
            row.append(filename)
            row.append("stage")
            row.append(vals["file_1"])
            row.append(vals.get("file_2", None))
            row = tuple(row)

            runtime_db.conn.execute(""" INSERT INTO runs
                                        VALUES (?,?,?,?,?)""",
                                    row)

            eprint("# Staged: " + filename)

        runtime_db.commit()

        # TODO: Listen for output

        if(args.loop):
            time.sleep(10)
        else:
            pipeline_p.start()
            pipeline_p.join(3600)
            cleanup(args.input_database)
            break
