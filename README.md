
# Use pipeline

Compile c code:
```bash
gcc -O3 -o fileEOF fileEOF.c
```

## Install docker images and databases

The pipeline requires that the docker images of the services used are installed (not only the services, but also in the version used by the pipeline).
Besides, the databases used by this programs are also required (kmerfinder_db, mlst_db, plasmidfinder_db,pmlst_db, pointfinder_db, resfinder_db,
virulencefinder_db). To do that, run the bash script **install_repositories** from where it is located.
```bash
#If you need the docker images and the databases
./install_repositories docker_db /path/to/kma_index
#If you only need the databases
./install_repositories db /path/to/kma_index
#If you only need the docker images
./install_repositories docker
```

## Usage


```python
usage: init_pipeline.py [-h] [-i INPUTDIR] [-idb INPUT_DATABASE] [-s STAGE]
                        [--loop] [--nextflow NEXTFLOW] [--no_cleanup] 
                        [-r RUNS] [-config CONFIG_FILE] [-d DATABASES_DIRECTORY]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUTDIR, --inputdir INPUTDIR
                        Directory to process
  -idb INPUT_DATABASE, --input_database INPUT_DATABASE
                        File to store sqlite3 db about input files.
  -s STAGE, --stage STAGE
                        Directory in which json files will be written.
  -d DATABASES_DIRECTORY, --databases_directory DATABASES_DIRECTORY
                        Directory containing the database folders
  --loop                Keep listening for new files until user kills process
                        with a SIGINT signal (ctrl+c)
  --nextflow NEXTFLOW   Path to nextflow
  --no_cleanup          Will not cleanup staged json files and sqlite database
  -r RUNS, --runs RUNS  Which system and how the pipeline work: TORQUE queueing,
                        local and manual
  -config CONFIG_FILE, --config_file CONFIG_FILE
                        Path to the nextflow config file for running the 
                        pipeline manually (-r manual)
  
```