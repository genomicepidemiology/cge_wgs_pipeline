results (1 - N):
	short (1 - N)
		column name/key>: <string>
		val: <string>
	main (0-1):
		type: <defined result type>
		result name/key <unique string> (0 - N): <defined result type>
service (1): name <string>
version (1): commit hash <string>
run date (1): <date> ?format?
run hash (1): <run hash>
run cmd (1): <command>
databases (0 - N)
	name: id <string>
	version: commit hash <string>
input id (1): <string>(sample name)




command:
	description: command used to execute service
	type: string

run hash:
	description: A run hash can be used to determine if different runs were run using the exact same parameters and using the exact same version of the databases
	create: Create a hash using ?SHA-256?. The hashed string should be created by concatenating (in order) the strings: [run cmd] + [databases:version](?lexicographical ordered?)



Result types:
Table
Usage:
	   Output_object.add_table(<nested dict>, name="<table name>", result="main")
	   Output_object.get(result="main", name="<table name>", row="header", column="header", default=None) return value or default
	   Output_object.short[key] = val
	   Output_object.main[table name1][row header][column key] = val
Description: Stores tables. Can be a simple table with or without (column/row) headers. I headers are not provided the header will default to a number (1-indexed).
Table:
{
	key: <table header>{
		key: <row header>{
			<column header>: <value>
		}
		key: "declarations"{
			<column name>: ["string", "int", "double", "bool [True|False]", "date [secs since epoc]"]
		}
	}
}

Value:
<column name>: value







Restults are nested dicts.
Define rows and columns.
Create table with a list defining column and row headers (eks. [c,r,c] or [column, row, column])


resfinder -> abclass -> drug -> gene

resfinder -> abclass -> gene

virulencefinder -> gene

mlstfinder -> ST

Example:
	abclass
	subj_length	query_length	ID
hit


???
Log packages etc?