#!/usr/local/anaconda/bin/python2.7
# #############################################################################
# #########                          CGE Service Wrapper              #########
# #############################################################################
# This script is part of the CGE Service structure
# --> The input/arguments are validated
# --> Create service folder and subfolders
# --> The uploaded files are copied to the 'Upload' directory
# --> Log service submission in SQL
# --> Setup and execution of service specific programs
# --> The success of the service is validated
# --> A HTML output page is created if in webmode
# --> Downloadable files are copied/moved to the 'Download' Directory
# --> The SQL service entry is updated
# --> The files are zipped for long-term storage
import sys
import os
import time
import random
import subprocess
import re

import pandas as pd

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append('/home/data1/services/CGEpipeline/CGEmodules')
from functions_module_2 import (printDebug, copyFile, program,
                                createServiceDirs, getArguments, paths,
                                makeFileList, fileUnzipper,
                                printInputFilesHtml, fileZipper,
                                PrepareDownload, UpdatePaths, proglist,
                                CheckFileType, printOut, moveFile, setDebug,
                                add2DatabaseLog, dlButton, GraceFullExit,
                                FixPermissions, tsv2html)


class program_hack(program):
   def GetCMD(self):
      ''' Overrides method in program object, to force execution of NextFlow
      '''
      cmd = self.path + " " + " ".join(self.args)
      return "/home/data1/tools/bin/nextflow/nextflow run " + cmd


# #############################################################################
# #########                              MAIN                         #########
# #############################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = 'NextFlowBAP', '0.1'

# No arguments from website, but needs to be called in order to access
# the hidden arguments.
args = getArguments('''''')

# VALIDATE REQUIRED ARGUMENTS
if(args.uploadPath is None
   or len(args.uploadPath) < 5 or not os.path.exists(args.uploadPath)):

    GraceFullExit('Error: No valid upload path was provided! (%s)\n'
                  % (args.uploadPath))

elif(args.uploadPath[-1] != '/'):
    args.uploadPath += '/'

# SET RUN DIRECTORY (No need to change this part)
if args.reuseid:
  runID = [x for x in args.uploadPath.split('/') if x != ''][-2]
else:
  runID = (time.strftime('%w_%d_%m_%Y_%H%M%S_') + '%06d'
           % random.randint(0, 999999))

paths.serviceRoot = '{programRoot}IO/%s/' % (runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
add2DatabaseLog(service + '-' + version, runID, args.usr, args.ip, "mixed")

# MOVE UPLOADS FROM APP TO ISOLATE UPLOAD DIRECTORY (No need to change)
if stat:
    # Move the uploaded files to the upload directory
    moveFile(args.uploadPath + '*', paths['uploads'])
    # GET INPUT FILES from input path
    inputFiles = makeFileList(paths['uploads'])
else:
    GraceFullExit('Error: Could not create upload directory!\n')


workDir = paths.serviceRoot
nextflowbap = program_hack(
    name="NextFlowBAP", path=(paths['scripts'] + 'bap.nf'), timer=0,
    ptype='/home/data1/tools/bin/nextflow/nextflow', toQueue=True,
    wait=False, workDir=workDir,
    args=['--fq1', inputFiles[0],
          '--fq2', inputFiles[1],
          '-with-report', 'report.html',
          '-with-trace',
          '-with-timeline', 'timeline.html',
          '-with-dag', 'dag.html'])

nextflowbap.Execute(True)

proglist.Add2List(nextflowbap)

# EXECUTION OF THE PROGRAMS
for progname in proglist.list:
  if proglist[progname].status == 'Executing':
     proglist[progname].WaitOn(silent=False)

for progname in proglist.list:
  status = proglist[progname].GetStatus()
  if(status != 'Done'):
    GraceFullExit("Error: Execution of the program failed!\n")

# CREATE THE HTML OUTPUT FILE (No need to change this part)
# if args.webmode:
#    if os.path.exists(paths['outputs']) and not os.path.exists(paths['outputs'] + service + '.out'):
#        with open(paths['outputs'] + service + '.out', 'w') as f:
#            f.write('<h1>%s-%s Server - Results</h1>' % (service, version))

# ##############################################################################
# LOG THE TIMERS (No need to change this part)
proglist.PrintTimers()

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug('Done')
