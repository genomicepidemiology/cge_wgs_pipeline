#!/usr/bin/env nextflow

package groovy.json
import java.io.File


Runtime.runtime.addShutdownHook {
  println "Shutting down..."
}

params.str = 'Hello world!'


in_channel = Channel
  .watchPath( '/home/alfred/Projects/cge_wgs_pipeline/src/python_module_seqfilehandler/test/stage/*.staged_json' )


process readStagedFile {
    input:
    val in_channel

    output:
    val json_file_1 into file1
    val json_file_2 into file2

    exec:
    println "File: ${in_channel}"
    fh = new File("${in_channel}")
    def jsonSlurper = new JsonSlurper()
    hash = jsonSlurper.parseText(fh.text)
    println "Hash: ${hash.file_1}"
    json_file_1 = hash.file_1
    json_file_2 = hash.file_2
}

process testHash {
    input:
    val file1
    val file2

    output:
    stdout test_results

    """
    printf 'TEST: ${file1}\n'
    printf 'TEST: ${file2}\n'
    """
}

process splitLetters {

    output:
    file 'chunk_*' into letters mode flatten

    """
    printf '${params.str}' | split -b 6 - chunk_
    """
}


process convertToUpper {

    input:
    file x from letters

    output:
    stdout result

    """
    cat $x | tr '[a-z]' '[A-Z]'
    """
}

result.subscribe {
    println it.trim()
}

test_results.subscribe {
    println it
}
