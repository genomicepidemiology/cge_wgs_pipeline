#!/usr/local/anaconda/bin/python2.7
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
#--> The input/arguments are validated
#--> Create service folder and subfolders
#--> The uploaded files are copied to the 'Upload' directory 
#--> Log service submission in SQL
#--> Setup and execution of service specific programs
#--> The success of the service is validated
#--> A HTML output page is created if in webmode
#--> Downloadable files are copied/moved to the 'Download' Directory
#--> The SQL service entry is updated
#--> The files are zipped for long-term storage
import sys, os, time, random, subprocess, re

# INCLUDING THE CGE MODULES (No need to change this part)
sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from assembly_module_2   import (PrepareAssembly, MakeAssembly,
                               printContigsInfo, AddAssemblyToProgList)
from functions_module_2  import (printDebug, copyFile, program, createServiceDirs,
                               getArguments, paths, makeFileList, fileUnzipper,
                               printInputFilesHtml, fileZipper, PrepareDownload,
                               UpdatePaths, proglist, CheckFileType, printOut, 
                               moveFile, setDebug, add2DatabaseLog, dlButton,
                               GraceFullExit, FixPermissions, tsv2html)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################

def tab2html_acquired(fp):
   #Printing the styles
   printOut("<style type='text/css'> .bglightgreen{background-color:#BEDCBE;} .bggrey{background-color:#9D9D9D;}.bggreen{background-color:#6EBE50;}.bgred{background-color:#BF2026;}.virresults td, th { border: 2px solid #FFFFFF; padding: 4px 16px 1px; text-align: left; vertical-align: middle;} .virresults th,td{font-size: 14px;padding: 4px 5px 1px 5px;text-align: center;} .virresults th{color:#FFF; background-color:#3956A6;}.virresults td a{color:#3956A6;}</style>")   
   printDebug('Writing HTML table for %s'%fp)
   if os.path.exists(fp):
      with open(fp, 'r') as f:
         intable = False
         tsize = 0
         ttitle = ''
         for l in f:
            l = l.strip("\n")
            if l == '':
               # Ending the tabe and making a new line
               printOut("</table>\n")
               printOut('<br>')
               intable = False
            elif re.search('^Resistance gene', l) != None and intable:
               tmp = l[0:].split('\t')
               tsize = len(tmp)
               # Print Header Line
               printOut("<tr><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
            elif intable:
               td = l.split('\t')
               # It the line are not tab seperated the line is just printed
               if (len(td) < 2):
                  printOut("<tr class='bgred'><td style='text-align:center'>%s</td></tr>\n"%(l))
               else:
                  ID = td[1]
                  tmp = td[2].split('/')
                  HSP = int(float(tmp[1]))
                  query = int(float(tmp[0]))
                  acc = td[-1]
                  td[-1] = "<a href='http://www.ncbi.nlm.nih.gov/nuccore/%s'> %s </a>"%(acc, acc)
                  
                  # Setting the background color
                  if (ID == '100.00') and (HSP == query):
                     printOut("<tr class='bggreen'>")
                  elif (ID != '100.00' and (HSP == query)):
                     printOut("<tr class='bglightgreen'>")
                  #elif (ID == '100.00' and (HSP != query)):
                  elif HSP != query:
                     printOut("<tr class='bggrey'>")
                  else:
                     printOut("<tr class='bgred'>")
                  #Printing the result
                  printOut("<td style='text-align:center'>%s</td>\n"%("</td><td style='text-align:center'>".join(td)))
            else:
               # Start table
               intable = True
               # Setting the table class
               printOut("<table class='center virresults' width='100%'>\n")
               printOut("<tr><th colspan='7'>%s</th></tr>\n"%(l))
         # END TABLE IF OPEN
         if intable: printOut("</table>\n")

def printExtendOutput_acquired(query, subject):
   #Define class
   #print("<style type='text/css'> .bglightgreen{background-color:#BEDCBE;} .bggrey{background-color:#9D9D9D;}.bggreen{background-color:#6EBE50;}.bgred{background-color:#BF2026;}.virresults td, th { border: 2px solid #FFFFFF; padding: 4px 16px 1px; text-align: left; vertical-align: middle;} .virresults th,td{font-size: 14px;padding: 4px 5px 1px 5px;text-align: center;} .virresults th{color:#FFF; background-color:#3956A6;}.virresults td a{color:#3956A6;}</style>")   
   printOut("<style type='text/css'> .bggrey{background-color: #AAAAAA;} .bgred{background-color: #BF2026;} .bggreen{background-color: #6EBE50;} tr.header{font-weight: bold; text-align: center; background-color: grey;} .w70{width:70%;} span, p{font-size: 16px;}</style>")

   
   # Print extended output button
   printOut("<script type='text/javascript'>function printOutput(tag){var ele = document.getElementById(tag).style;if (ele.display=='block'){ele.display='none';}else{ele.display='block';}}</script>\n")
   printOut("<center><button type='button' onclick='printOutput(&quot;eo&quot;)'>extended output</button></center><div id='eo' class='hide'>")
   
   # Start output
   printOut("<pre><br>")
   
   # Get output
   if os.path.exists(query) and os.path.exists(subject):
      with open(query, 'r') as q, open(subject, 'r') as s:
            # Reading in the reference lines
            sl = s.readlines()
            
            # Predefining variables
            nrGene = 2
            nrLine = 0
            startPos = 0
            endPos = 0
            curPos = 0
            ID = 0
            HSP = 0
            allele = 0
            
            # Going through the query file
            for ql in q:
               ql = ql.strip("\n")
               sline = sl[nrLine].strip("\n")
               
               if re.search('^>', ql) != None:
                  printOut('<br><br>')
                  outStr = str(ql[1:])
                  match = re.search('.*ID:\s*(\d+\.*\d*).*HSP/Length:\s+(\d+)/(\d+).*', outStr)
                  ID = match.group(1)
                  HSP = int(float(match.group(2)))
                  allele = int(float(match.group(3)))
                  
                  # Printing the output depending on it being a perfect match or not
                  if (ID == '100.00') and (HSP == allele):
                     printOut(("<span style='font-size:15px; color:grey'>%s</span><br>")%(outStr))
                  else:
                     printOut(("<span style='font-size:15px; color:grey'>%s</span><br>")%(outStr))
                     m = re.search('.*Positions\sin\sreference:\s*(\d+)..(\d+).*', outStr)
                     startPos = int(float(m.group(1)))
                     endPos = int(float(m.group(2)))
                     curPos = 0
                  # Increasing the line number and gene number by one
                  nrGene += 1
                  nrLine += 1
               else:
                  if ID == '100.00' and (HSP == allele):
                     # Printing the whole alignment in green
                     printOut(("<span style='font-size:14px; color:black'>Resistance gene seq: </span><span class='bggreen'>%s</span>")%(sline))
                     printOut(("<span style='font-size:14px; color:black'>Hit in genome:       </span><span class='bggreen'>%s</span><br><br>")%(ql))
                  else:
                     # Creating a string with the html code
                     finalVirLine = "<br><br><span style='font-size:14px; color:black'>Resistance gene seq: </span>"
                     finalHitLine = "<br><span style='font-size:14px; color:black'>Hit in genome:       </span>"
                     
                     # Going through every base and defining the color
                     pos = 0
                     for char in ql:
                        skip = 0
                        # Getting the reference base unless the query is longer than the reference
                        if pos > (len(sline) - 1):
                           pos += 1
                           skip = 1
                        else: 
                           sChar = sline[pos]
                        
                        # If the posistion is not included in the alignment the background is gray
                        # if the query is longer than the refernece then the posisions are not inlcuded
                        if skip == 1:
                           pass
                        elif (curPos < (startPos - 1)) or (curPos > (endPos - 1)):
                           color = 'grey'
                        else:
                           # If the bases align the color is green ortherwise it is red
                           if char == sChar:
                                 color = 'green'
                           else:
                                 color = 'red'
                        
                        # Adding the base with the right color
                        finalVirLine += "<span class='bg%s'>%s</span>"%(color, sChar)
                        finalHitLine += "<span class='bg%s'>%s</span>"%(color, char)
                        pos += 1
                        curPos += 1
                     
                     # Printing the alignmet lines.
                     printOut(('%s%s')%(finalVirLine,finalHitLine))
                  nrLine += 1
   
   # End output
   printOut("</pre><br><br></div>")

def tab2html_point(tab):
  printOut("<style type='text/css'>.bggreen{background-color:#6EBE50;} /*PERFECT HIT COLOR*/.bgred{background-color:#BF2026;} /*NO HITS COLOR*/.results th,td{ /*PROPERTIES OF TABLE CELLS*/font-size: 14px;padding: 4px 5px 1px 5px;text-align: center;}.results th{ /*HEADER COLORS*/color:#FFF;background-color:#4169E1;}.results td a{color:#4169E1;} /*LINK COLOR*/</style>")
  
  printDebug('Writing HTML table for %s'%tab)
  if os.path.exists(tab):
     with open(tab, 'r') as f:
        intable = False
        header = ''
        for l in f:
           l = l.strip("\n")
           if re.search('^Chromosomal point', l) != None or l.startswith("Known") or l.startswith("Unknown"):
              printOut("<h2> %s </h2>"%(l))
           elif re.search('^Species', l) != None:
              td = l.split(":")
              printOut("<h3>%s:<i class='grey'>%s</i></h3>"%(td[0], td[1]))
           elif l == '':
              # Ending the tabe and making a new line
              if intable: printOut("</table>\n")
              printOut('<br>')
              intable = False
           elif re.search('^Mutation', l) != None and intable:
              tmp = l[0:].split('\t')
              header = tmp
              # Print Header Line
              printOut("<tr class='bggrey'><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
           elif intable:
              l = l.replace("->","&#10141")
              td = l.split('\t')
              # It the line are not tab seperated the line is just printed
              if (len(td) < 2):
                 printOut("<tr class='bggrey'><td style='text-align:center'>%s</td></tr>\n"%(l))
              else:
                 if 'PMID' in header and (td[-1] != "No" or td[-1] != "-"):
                  acc = td[-1]
                  td[-1] = "<a href='http://www.ncbi.nlm.nih.gov/pubmed/%s'> %s </a>"%(acc, acc)
                 printOut("<tr class='bggreen'><td style='text-align:center'>%s</td></tr>\n"%("</td><td style='text-align:center'>".join(td)))
           else:
              # Start table
              intable = True
              # Setting the table class
              printOut("<table class='center results' width='100%'>\n\n")
              printOut("<tr><th colspan='5'>%s</th></tr>\n"%(l))
        # END TABLE IF OPEN
        if intable: printOut("</table>\n")


################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES
setDebug(False)
service, version = "ResFinder", "3.0"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
#(OPTION,   VARIABLE,  DEFAULT,  HELP)
#args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments('''
selectionbox    technology   -t      VALUE
selectionbox    threshold    -T      VALUE
selectionbox    anti         -a      VALUE
selectionbox    minlength    -L      VALUE
selectionbox    species      -S      VALUE
checkbox        point        -c      VALUE
checkbox        acquired     -acq    VALUE
selectionsbox   showmut      -U      VALUE
''', allowcmd=True)


#----DELETE THIS----#
# VALIDATE REQUIRED ARGUMENTS
if args.technology == None: GraceFullExit("Error: No technology platform was chosen!\n")
if args.uploadPath is None or len(args.uploadPath) < 5 or not os.path.exists(args.uploadPath):
   GraceFullExit("Error: No valid upload path was provided! (%s)\n"%(args.uploadPath))
elif args.uploadPath[-1] != '/': args.uploadPath += '/' # Add endslash
#-------------------#

# SET RUN DIRECTORY (No need to change this part)
if args.reuseid:
  #/srv/www/secure-upload/isolates/5_16_4_2015_162_299_423438//0/
  runID = [x for x in args.uploadPath.split('/') if x!=''][-2]
else:
  runID = time.strftime('%w_%d_%m_%Y_%H%M%S_')+'%06d'%random.randint(0,999999)
paths.serviceRoot = '{programRoot}IO/%s/'%(runID)
paths.isolateRoot = '{programRoot}'
paths.add_path('uploads', '{serviceRoot}Uploads/')

# SET AND UPDATE PATHS (No need to change this part)
UpdatePaths(service, version, '', '', args.webmode)
databases = "/home/data1/services/%s/pointfinder_db"%(service)
blast = "/home/data1/tools/bin/blastn"

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create('uploads')

# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
add2DatabaseLog(service+'-'+version, runID, args.usr, args.ip, args.technology)

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
   # Move the uploaded files to the upload directory
   moveFile(args.uploadPath+'*', paths['uploads'])
   # Unzipping uploaded files if zipped
   fileUnzipper(paths['uploads'])
   # GET INPUT FILES from input path
   inputFiles = makeFileList(paths['uploads'])
else:
   GraceFullExit("Error: Could not create upload directory!\n")

#
#----CHANGE THIS----#
#
# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
if args.technology != 'Assembled_Genome':
   # ADD THE ASSEMBLER to the program list
   AddAssemblyToProgList()
   # Prepare the Assembly program for execution
   PrepareAssembly(args.technology, inputFiles)
   # Assemble the reads into contigs
   n50 = MakeAssembly(args.technology)
   # The success of the assembler is validated
   status = proglist['Assembler'].GetStatus()
   if status != 'Done' or not isinstance(n50, int):
      GraceFullExit("Error: Assembly of the inputfile(s) failed!\n")#%(len(inputFiles)))
else:
   # Validate that the uploaded file is fasta
   if CheckFileType(inputFiles[0]) != 'fasta':
      GraceFullExit("Error: Invalid contigs format (%s)!\nOnly the fasta format is recognised as a proper contig format.\n"%(CheckFileType(inputFiles[0])))
   # Validate that only 1 file was submitted
   if len(inputFiles) != 1:
      GraceFullExit("Error: Invalid number of contig files. Only one fasta file is allowed.\n")#%(len(inputFiles)))
   # Add contigsPath
   paths.add_path('contigs', paths['inputs']+'contigs.fsa')
   # Copy file to Input directory
   copyFile(inputFiles[0], paths['contigs'])
#
#-------------------#
#

if not (args.acquired) and not (args.point):
  args.acquired = True

# ADDING PROGRAMS
if (args.acquired):
  name = "RF"
  workDir = "%s/%s"%(paths.serviceRoot,name)
  os.mkdir(workDir)
  resfinder = program(
   name=name, path=paths['scripts']+'ResFinder-3.0.pl', timer=0,
   ptype='perl', toQueue=True, wait=False, workDir=workDir,
   args=['-i', paths['contigs'],
         '-k', args.threshold,
         '-l', args.minlength,
         '-d', args.anti,
         '-R', paths.serviceRoot])
  resfinder.Execute(True)
  proglist.Add2List(resfinder)

if (args.point):
  name = "PF"
  workDir = "%s/%s"%(paths.serviceRoot,name)
  os.mkdir(workDir)
  pointmutations = program(
    name=name, path=paths['scripts']+'PointFinder-3.0.py', timer=0,
    ptype='/home/data1/tools/bin/anaconda/bin/python', toQueue=True, wait=False, workDir=workDir,
    args=['-i', paths['contigs'],
          '-o', paths['outputs'],
          '-s', args.species,
          '-p', databases,
          '-b', blast])
  
  if args.showmut == 'True':
     pointmutations.AppendArgs(['-u'])
  pointmutations.Execute(True)
  proglist.Add2List(pointmutations)

# EXECUTION OF THE PROGRAMS
for progname in proglist.list:
  if proglist[progname].status == 'Executing':
     proglist[progname].WaitOn(silent=False)

for progname in proglist.list:
  status = proglist[progname].GetStatus()
  if status != 'Done': GraceFullExit("Error: Execution of the program failed!\n")

### PRINT HTML ###

printOut('<h1>%s-%s Server - Results</h1>'%(service, version))

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
if len(inputFiles) > 0: printInputFilesHtml(inputFiles)
  
# PRINT THE CONTIGS INFORMATION TO THE HTML OUTPUT FILE
if args.technology != 'Assembled_Genome': printContigsInfo(runID=runID)

if (args.acquired):
  # Print output button
  printOut("<script type='text/javascript'> "+
           "function printFunction(id) {"+
           "var e = document.getElementById(id); "+
           "if(e.style.display == 'inline') "+
           "e.style.display = 'none'; "+
           "else "+
           "e.style.display = 'inline'; "+
           "}</script>")
  printOut("<center><button type='button' onclick='printFunction(&quot;eo1&quot;)'> "+
           "Show Acquired antimicrobial resistance results</button></center> "+
           "<div id='eo1' class='hide'>")
  
  # Start output
  printOut('<h2>Acquired antimicrobial resistance gene - Results</h2>')
  ## PREPARE THE DOWNLOADABLE FILE(S)
  _ = PrepareDownload(paths['serviceRoot']+'downloads/results.txt')
  _ = PrepareDownload(paths['serviceRoot']+'downloads/Hit_in_genome_seq.fsa')
  _ = PrepareDownload(paths['serviceRoot']+'downloads/Resistance_gene_seq.fsa')
  _ = PrepareDownload(paths['serviceRoot']+'downloads/results_tab.txt')
  
  ## PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
  tab=paths['downloads']+'results_tab.txt'
  if os.path.exists(tab):
    tab2html_acquired(tab)
    printOut('<br>\n')
  
  # GETTING THE MINIMUM LENGTH FOR OUTPUT PRINT
  minlength = float(args.minlength) * 100
  threshold = int(float(args.threshold))
  
  # PRINT THE SETTINGS
  printOut("<h2>Selected %%ID threshold: &nbsp;<i class='grey'>%d %%</i></h2>"%(threshold))
  printOut("<h2>Selected minimum length: &nbsp;<i class='grey'>%d %%</i></h2>"%(minlength))
  
  ## PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
  dlButton('Results as text', 'results.txt')
  dlButton('Hit in genome sequences', 'Hit_in_genome_seq.fsa')
  dlButton('Resistance gene sequences', 'Resistance_gene_seq.fsa')
  dlButton('Results as tabseperated file', 'results_tab.txt')
  
  ## PRINT THE EXTENDED OUTPUT
  ref=paths['downloads']+'Resistance_gene_seq.fsa'
  hit=paths['downloads']+'Hit_in_genome_seq.fsa'
  if os.path.exists(ref) and os.path.exists(hit):
    printExtendOutput_acquired(hit, ref)
    printOut('<br>\n')
  # End output
  printOut("<br><br></div>")

if (args.point):
  # Print output button
  printOut("<script type='text/javascript'> "+
           "function printFunction(id) { "+
           "var e = document.getElementById(id); "+
           "if(e.style.display == 'inline') "+
           "e.style.display = 'none'; "+
           "else "+
           "e.style.display = 'inline'; "+
           "}</script>")
  printOut("<center><button type='button' onclick='printFunction(&quot;eo2&quot;)'> "+
           "Show Point mutation results</button></center> "+
           "<div id='eo2' class='hide'>")
  
  # Start output
  printOut("<br>")  
  ## PRINT TAB FILE(S) AS RESULT TABLES TO THE HTML OUTPUT FILE
  point=paths['outputs']+'PointFinder_table.txt'
  if os.path.exists(point):
    tab2html_point(point)
    printOut('<br>\n')
  
  ## PRINT FILE DOWNLOAD-BUTTON(S) TO THE HTML OUTPUT FILE
#"PointFinder_results.txt", "PointFinder_table.txt"
  _ = PrepareDownload(paths['serviceRoot']+'outputs/PointFinder_table.txt')
  dlButton('Download the result table', 'PointFinder_table.txt')
  
  _ = PrepareDownload(paths['serviceRoot']+'outputs/PointFinder_results.txt')
  dlButton('Download tabseperated results', 'PointFinder_results.txt')

  printOut("<br><br></div>")

################################################################################
# LOG THE TIMERS (No need to change this part)
proglist.PrintTimers() 

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
fileZipper(paths['serviceRoot'])
