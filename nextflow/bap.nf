#!/usr/bin/env nextflow

/*
  Paths to pipeline software
*/
anaconda3 = "/home/data1/tools/bin/anaconda3/bin/python"
assembler = "/home/data1/services/SalmonellaTypeFinder/SalmonellaTypeFinder-1.3/scripts/food_qc_pipeline/FoodQCPipeline_v1.2.py"
kmerfinder = "/home/data1/services/KmerFinder/KmerFinder-3.0/scripts/kmerfinder/kmerfinder.py"
mlst = "/home/data1/services/MLST/MLST-2.0/scripts/mlst/mlst.py"
resfinder = "/home/data1/services/ResFinder/ResFinder-3.0/scripts/ResFinder-3.0.pl"
pointfinder = "/home/data1/services/ResFinder/ResFinder-3.0/scripts/PointFinder-3.0.py"
salmonellatypefinder = "/home/data1/services/SalmonellaTypeFinder/SalmonellaTypeFinder/scripts/SalmonellaTypeFinder.py"

params.contigs = "/home/rkmo/test/contigs.fsa"









process findSpecies {

  output:
  file "data.json" into species_json

  """
  python3 $kmerfinder \
  -db /home/data2/databases/internal/kma/kmerdb/latest/bacteria/bacteria.ATG \
  -tax /home/data2/databases/internal/kma/taxdb/latest/bacteria/bacteria.name \
  -kp /home/data2/databases/internal/kma/src/kma/ \
  -x \
  -o . \
  -i '${params.fq1} ${params.fq2}'
  """
}


process extractSpecies {

  executor 'local'

  input:
  file species_json

  output:
  stdout species

  """
  #!/usr/bin/env python3

  import json

  with open("$species_json", "r", encoding="utf-8") as json_fh:
    species_dict = json.load(json_fh)

  best_score = 0
  species = "unknown"

  for hit, val in species_dict["kmerfinder"]["results"]["species_hits"].items():

    score = int(val["Score"])

    if(score > best_score):
      best_score = score
      species = val["Species"]

  print(species, end='')
  """
}


process mlst {

  input:
  val species

  output:
  file "data.json" into mlst_json

  """
  MLST_SPECIES=`grep "$species" /home/data1/services/MLST/database_repository/config | cut -f 1 -`

  python3 $mlst \
  -s \$MLST_SPECIES \
  -p "/home/data1/services/MLST/database_repository/" \
  -t . \
  -mp /home/data2/databases/internal/kma/src/kma/kma \
  -x \
  -o . \
  -i '${params.fq1}'
  """

}


process extractMLST {

  executor 'local'

  input:
  file mlst_json

  output:
  stdout sequence_type

  """
  #!/usr/bin/env python3

  import json

  with open("$mlst_json", "r", encoding="utf-8") as json_fh:
    mlst_dict = json.load(json_fh)

  st = mlst_dict["mlst"]["results"]["sequence_type"]

  print(st, end='')
  """
}


process salmonellatypefinder {

  input:
  val species

  script:
  if(species == 'Salmonella enterica')
    """
    $anaconda3 $salmonellatypefinder ${params.fq1} ${params.fq2}
    """
  else
    """
    echo "no serotype for species $species" > typeFinderResults.txt
    """
}


process assembler {
  container 'some_docker_image_name'

  output:
  file 'Assemblies/*.fa' into assemblies

  """
    $anaconda3 $assembler ${params.fq1} ${params.fq2} \
    --queue None \
    --spades True \
    --grade True \
  """
}


process resfinder {

  input:
  file assemblies

  """
  perl $resfinder \
  -k 80.00 \
  -l 60.00 \
  -d "aminoglycoside,beta-lactam,colistin,quinolone,fosfomycin,fusidicacid,glycopeptide,macrolide,nitroimidazole,oxazolidinone,phenicol,rifampicin,sulphonamide,tetracycline,trimethoprim" \
  -i '$assemblies'
  """
}


process extractPointfinderSpecies {

  input:
  val species

  output:
  stdout pf_species

  """
  #!/usr/bin/env python3

  species_dict = {
    "Salmonella enterica": "salmonella",
    "Escherichia coli": "e.coli",
    "Campylobacter jejuni": "campylobacter",
    "Neisseria gonorrhoeae": "gonorrhoeae",
    "Mycobacterium tuberculosis": "tuberculosis"
  }

  if("$species" in species_dict):
    print(species_dict["$species"], end = '')
  else:
    print("NA")
  """
}


process pointfinder {

  input:
  val pf_species
  file assemblies

  script:
  if( pf_species != 'NA' )
    """
    python $pointfinder \
    -s $pf_species \
    -p /home/data1/services/ResFinder/pointfinder_db \
    -b "/home/data1/tools/bin/blastn" \
    -o . \
    -i $assemblies
    """
}
