#!/usr/bin/env nextflow



params.fq1 = "1706F19609_S69_L555_R1_001.fastq.gz"
params.fq2 = "1706F19609_S69_L555_R2_001.fastq.gz"
params.wordir = "/home/rkmo"
params.dbdir = "/home/rkmo/src/kmerdbs"



process findSpecies {
  container 'kmerfinder'
  runOptions = "-v ${params.wordir}:/workdir -v ${params.dbdir}:/database"

  output:
  file "data.json" into species_json

  """
  kmerfinder -i '${params.fq1} ${params.fq2}' -o . -db /database/bacteria.ATG -tax /database/bacteria.name -x
  """
}
